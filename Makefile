all: Start Case Game CaseState GameState CaseCoordinate CaseManager GameManager CaseHandler CustomAction GameHandler MenuHandler CustomButton DigitField LayerPanel MovePanel SaveUtils MenuFrame MainMenu NewGameMenu IMenu
	cp -R src/images bin/
	cp -R src/font bin/
	jar -cvfm demineur.jar MANIFEST.MF fr -C bin .
	
#Remplacer le : par ; pour executer sous windows
SEPARATOR=:
	
test:
	java -jar demineur.jar

Start : MenuFrame
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/main/Start.java
	
#View

Case : CaseCoordinate GameState CaseState
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/Case.java
Game : CaseHandler CustomAction GameHandler GameManager GameState CustomButton LayerPanel MovePanel SaveUtils
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/Case.java

#Model

CaseState : 
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/model/CaseState.java
GameState : 
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/model/GameState.java
CaseCoordinate : 
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/model/CaseCoordinate.java
CaseManager : Case
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/model/CaseManager.java
GameManager : Case
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/model/GameManager.java

#Controller

CaseHandler : CaseManager CaseState GameState Case
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/controller/CaseHandler.java
CustomAction : MainMenu NewGameMenu
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/controller/CustomAction.java
GameHandler : Game MainMenu CustomButton SaveUtils
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/controller/GameHandler.java
MenuHandler : GameManager Game IMenu NewGameMenu CustomButton SaveUtils
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/controller/MenuHandler.java
	
#Utils

CustomButton : CustomAction
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/utils/CustomButton.java
DigitField :
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/utils/DigitField.java
LayerPanel : CustomAction GameState Game
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/utils/LayerPanel.java
MovePanel : IMenu
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/utils/MovePanel.java
SaveUtils : CaseState GameState GameManager Case Game
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/utils/SaveUtils.java
	
#Menu

MenuFrame :
	mkdir bin
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/menu/MenuFrame.java
MainMenu : CustomAction MenuHandler CustomButton MovePanel
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/menu/MainMenu.java
NewGameMenu : CustomAction MenuHandler CustomButton DigitField MovePanel
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/menu/NewGameMenu.java
	
#Interface

IMenu :
	javac -d bin -cp src$(SEPARATOR)junit.jar src/fr/dehoux/view/menu/IMenu.java