package fr.dehoux.model;

import fr.dehoux.view.Case;

import java.util.List;

/**
 * La classe <code>GameManager</code> permet de gérer tout les calculs liés au jeu ainsi que de
 * stockées différentes données qui permettent de caractériser une partie.
 *
 * @author Kevin Dehoux
 * @version 1.0
 */
public class GameManager {
    /**
     * Nombre de lignes.
     */
    private int rows;

    /**
     * Nombre de colonnes.
     */
    private int colons;

    /**
     * Nombre de bombes.
     */
    private int bombs;

    /**
     * A été chargé depuis une sauvegarde ou pas.
     */
    private boolean saveLoaded;

    /**
     * Manager / modèles des cases (<code>CaseManager</code>)
     */
    public CaseManager caseManager = new CaseManager();

    /**
     * Constructeur destiné à initialisé le nombre de lignes/colonnes/bombes, ainsi que
     * la génération des cases et des bombes.
     *
     * @param rows Nombre de lignes.
     * @param colons Nombre de colonnes.
     * @param bombs Nombre de bombes.
     * @param saveLoaded Chargé depuis une sauvegarde ou pas.
     */
    public GameManager(int rows, int colons, int bombs, boolean saveLoaded) {
        this.rows = rows;
        this.colons = colons;
        this.bombs = bombs;
        this.saveLoaded = saveLoaded;

        generateCases();
        generateBombs();
    }

    /**
     * Renvoie le nombre de lignes.
     *
     * @return Le nombre de lignes.
     */
    public int getRows() {
        return this.rows;
    }

    /**
     * Renvoie le nombre de colonnes.
     *
     * @return Le nombre de colonnes.
     */
    public int getColons() {
        return this.colons;
    }

    /**
     * Renvoie le nombre de bombrd.
     *
     * @return Le nombre de bombes.
     */
    public int getBombs() {
        return this.bombs;
    }

    /**
     * Renvoie vrai ou faux si la partie est chargé depuis un sauvegarde ou pas.
     *
     * @return true/false.
     */
    public boolean isSaveLoaded() {
        return saveLoaded;
    }

    /**
     * Génère les cases en fonction du nombre de lignes/colonnes.
     *
     * @see CaseManager
     */
    private void generateCases() {
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < colons; y++) {
                this.caseManager.registerCase(new Case(x, y));
            }
        }
    }

    /**
     * Génère les bombes aléatoirement en fonction du nombre de bombes.
     *
     * @see CaseManager
     */
    private void generateBombs() {
        for(int i = 0; i < bombs;) {
            Case c = this.caseManager.getRandomCase(this.rows, this.colons);
            if(!c.isMined()) {
                c.setMined(true);
                i++;

                List<Case> listCase = this.caseManager.getCasesNeighbour(c);
                for (Case neighbourCase : listCase) {
                    if(neighbourCase != null) {
                        neighbourCase.addBombsNearby();
                    }
                }
            }
        }
    }
}
