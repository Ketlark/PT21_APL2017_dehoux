package fr.dehoux.model;

/**
 * La classe <code>CaseCoordinate</code> est utilisé pour assigner des coordonnées à une case (<code>Case</code>)
 * elle contient 2 composantes : x et y.
 *
 * @version 1.0
 * @author Kevin Dehoux
 */
public class CaseCoordinate {
    private int xCoord;
    private int yCoord;

    /**
     * Constructeur destiné à initialiser les 2 composantes.
     *
     * @param x Coordonné sur la ligne
     * @param y Coordonné sur la colonne
     */
    public CaseCoordinate(int x, int y) {
        this.xCoord = x;
        this.yCoord = y;
    }

    /**
     * Renvoie la composante X correspondant a la position sur une ligne.
     *
     * @return La composante X.
     */
    public int getX() {
        return xCoord;
    }

    /**
     * Permet de définir la composante X.
     *
     * @param x Integer
     */
    public void setX(int x) {
        this.xCoord = x;
    }

    /**
     * Renvoie la composante Y correspondant a la position sur une colonne.
     *
     * @return La composante Y.
     */
    public int getY() {
        return yCoord;
    }

    /**
     * Permet de définir la composante Y.
     *
     * @param y Integer
     */
    public void setY(int y) {
        this.yCoord = y;
    }

    /**
     * Permet d'évaluer deux objets <code>CaseCoordinate</code> pour savoir
     * si ils sont identiques ou pas.
     *
     * @param obj <code>CaseCoordinate</code>
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof CaseCoordinate && xCoord == ((CaseCoordinate) obj).xCoord && yCoord == ((CaseCoordinate) obj).yCoord;
    }

    /**
     * @return L'identifiant de l'objet <code>CaseCoordinate</code>
     */
    @Override
    public int hashCode() {
        return (xCoord+10) * (yCoord+3);
    }
}
