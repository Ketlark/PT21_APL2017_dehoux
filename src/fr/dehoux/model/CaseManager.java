package fr.dehoux.model;

import fr.dehoux.view.Case;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * La classe CaseManager permet de gérer tout les calculs liés aux cases du jeu, elle stock aussi différentes
 * liste de cases utilisés dans la vue et certains contrôleurs.
 *
 * @author Kevin Dehoux
 * @version 1.0
 */
public class CaseManager {

    /**
     * Liste contenant toutes les cases (<code>Case</code>) comme valeur, et leurs coordonnées comme clé (<code>CaseCoordinate</code>).
     */
    private HashMap<CaseCoordinate, Case> casesMap = new HashMap<>();

    /**
     * Liste contenant les cases (<code>Case</code>) taggées "étoile".
     */
    private List<Case> caseTagged = new LinkedList<>();

    /**
     * Liste contenant les cases (<code>Case</code>) révélées.
     */
    private List<Case> caseRevealed = new LinkedList<>();

    /**
     * Objet random servant à déterminer une case aléatoire dans
     * la fonction <code>getRandomCase</code>.
     */
    private Random random = new Random();

    /**
     * Nombre de lignes.
     */
    private int rows;

    /**
     * Nombre de colonnes.
     */
    private int colons;

    /**
     * Fonction qui retourne la liste de toutes les cases du jeu.
     *
     * @return HashMap<CaseCoordinate   ,       Case> contenant toutes les cases (<code>Case</code>) du jeu.
     */
    public HashMap<CaseCoordinate, Case> getCasesMap() {
        return this.casesMap;
    }

    /**
     * Fonction qui retourne la liste de toutes les cases taggées "étoile" du jeu.
     *
     * @return List<Case> contenant toutes les cases taggées "étoile" (<code>Case</code>) du jeu.
     */
    public List<Case> getCaseTagged() {
        return caseTagged;
    }

    /**
     * Fonction qui retourne la liste de toutes les cases révélées du jeu.
     *
     * @return List<Case> contenant toutes les cases révélées (<code>Case</code>) du jeu.
     */
    public List<Case> getCaseRevealed() {
        return caseRevealed;
    }

    /**
     * Fonction permettant d'enregistrer une case dans la liste des cases du jeu avec comme clé ses coordonnées sous
     * forme d'un objet <code>CaseCoordinate</code>.
     *
     * @param c (<code>Case</code>)
     * @see Case
     * @see CaseCoordinate
     */
    public void registerCase(Case c) {
        this.casesMap.put(c.getCoord(), c);
    }

    /**
     * Permet de récupérer une case à partir de ses coordonnées
     *
     * @see Case
     *
     * @param x Composante X de la case.
     * @param y Composante X de la case.
     * @return Une case enregistré dans <code>casesMap</code> correspondant aux coordonnées indiqués.
     */
    private Case getCaseWithLocation(int x, int y) {
        return this.casesMap.get(new CaseCoordinate(x, y));
    }

    /**
     * Permet de récupérer une case aléatoire
     *
     * @see Case
     *
     * @param rows Nombre de lignes du jeu.
     * @param colons Nombre de colonnes du jeu.
     * @return Une case aléatoire parmis celles enregistrées dans <code>casesMap</code>.
     */
    public Case getRandomCase(int rows, int colons) {
        this.rows = rows;
        this.colons = colons;

        int x = random.nextInt(rows);
        int y = random.nextInt(colons);
        return this.getCaseWithLocation(x, y);
    }

    /**
     * Permet de récupérer les 8 cases voisines de celle indiqué en paramètre.
     *
     * @param c (<code>Case</code>)
     * @return Une liste de cases (<code>Case</code>)
     */
    public List<Case> getCasesNeighbour(Case c) {
        List<Case> caseList = new LinkedList<>();

        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {

                int coordX = c.getCoord().getX() + i;
                int coordY = c.getCoord().getY() + j;

                if (i == 0 && j == 0) continue;
                if (coordX < 0 || coordY < 0) continue;

                Case step = this.getCaseWithLocation(coordX, coordY);
                caseList.add(step);
            }
        }
        return caseList;
    }

    /**
     * Permet de récupérer les 4 cases adjacente de celle indiqué en paramètre.
     *
     * @param c (<code>Case</code>)
     * @return Une liste de cases (<code>Case</code>)
     */
    public List<Case> getCasesAdjacent(Case c) {
        List<Case> caseList = new LinkedList<>();
        int coordX = c.getCoord().getX();
        int coordY = c.getCoord().getY();

        int nord = coordY - 1;
        int sud = coordY + 1;
        int est = coordX + 1;
        int ouest = coordX - 1;

        if (nord >= 0) caseList.add(getCaseWithLocation(coordX, nord));
        if (sud <= colons) caseList.add(getCaseWithLocation(coordX, sud));
        if (est <= rows) caseList.add(getCaseWithLocation(est, coordY));
        if (ouest >= 0) caseList.add(getCaseWithLocation(ouest, coordY));

        return caseList;
    }


    /**
     * Fonction récursive qui permet de dévoiler les cases autour d'une case si celle ci est vide.
     *
     * @param c (<code>Case</code>)
     */
    public void loopReveal(Case c) {
        List<Case> caseToCheck = this.getCasesNeighbour(c);

        for (Case caseScope : caseToCheck) {
            if (caseScope != null && !caseScope.isClicked()) {

                caseScope.setClicked(true);
                this.getCaseRevealed().add(caseScope);

                if (caseScope.getBombsNearby() == 0) {
                    loopReveal(caseScope);
                }
            }
        }
    }
}
