package fr.dehoux.model;

import java.io.Serializable;

/**
 * La classe <code>GameState</code> est une énumération permettant d'assigner un état à une partie.
 *
 * @author Kevin Dehoux
 * @version 1.0
 */
public enum GameState implements Serializable {
    /**
     * Partie toujours en cours.
     */
    INGAME,

    /**
     * Partie terminé.
     */
    FINISHED,

    /**
     * Partie gagné.
     */
    VICTORY,

    /**
     * Partie perdu.
     */
    DEFEAT;
}
