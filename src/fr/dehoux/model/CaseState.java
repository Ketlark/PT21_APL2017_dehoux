package fr.dehoux.model;

import java.io.Serializable;

/**
 * La classe <code>CaseState</code> est une énumération permettant d'assigner un état de tag à une case.
 *
 * @author Kevin Dehoux
 * @version 1.0
 */
public enum CaseState implements Serializable {
    /**
     * Etat neutre.
     */
    NONE(0, "NONE"),

    /**
     * Etat tag "étoile"
     */
    TAGGED(1, "TAGGED"),

    /**
     * Etat tag "?"
     */
    IDK(2, "IDK");

    /**
     * ID de la valeur
     */
    private int id;

    /**
     * Nom de la valeur
     */
    private String name;

    /**
     * Constructeur destiné à initialiser tous les états.
     *
     * @param id ID de la valeur
     * @param name Nom de la valeur
     */
    CaseState(int id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * Retourne l'ID de la valeur.
     *
     * @return L'ID de la valeur.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Fonction permettant de connaître le prochain état à partir de l'actuel.
     *
     * @param s l'état actuel <code>CaseState</code>
     * @return Le prochain état <code>CaseState</code>
     */
    public static CaseState nextState(CaseState s) {
        if(s.getId() == 2) {
            return NONE;
        } else {
            return values()[s.getId() + 1];
        }
    }

    /**
     * Retourne le nom de la valeur.
     *
     * @return Le nom de la valeur.
     */
    @Override
    public String toString() {
        return this.name;
    }
}