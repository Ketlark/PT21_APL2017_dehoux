package fr.dehoux.controller;

import fr.dehoux.view.Game;
import fr.dehoux.view.menu.MainMenu;
import fr.dehoux.view.utils.CustomButton;
import fr.dehoux.view.utils.SaveUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * La classe <code>GameHandler</code> est utilisé est utilisé comme contrôleur, elle permet d'éxécuter des actions sur les boutons qui se trouve dans la vue du jeu (<code>Game</code>)
 * en fonction du bouton présent.
 */
public class GameHandler implements ActionListener, MouseListener {

    /**
     * Vue du jeu. (<code>Game</code>)
     */
    private Game g;

    /**
     * Constructeur destiné à initialisé la vue.
     *
     * @param game Vue du jeu (<code>Game</code>).
     */
    public GameHandler(Game game) {
        this.g = game;
    }


    /**
     * Exécute l'action assigné au bouton <code>CustomButton</code> lorsque celui-ci est cliqué.
     *
     * @see ActionEvent
     * @see CustomButton
     * @see CustomAction
     *
     * @param e (<code>ActionEvent</code>)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() instanceof CustomButton) {
            CustomAction action = ((CustomButton) e.getSource()).getCustomAction();

            if(action == CustomAction.CLOSE) {
                SaveUtils.readGame();
                System.exit(0);
            }

            if(action == CustomAction.SAVE_GAME) {
                SaveUtils.saveGame(g);
                returnToMenu();
            }

            if(action == CustomAction.RETURN_MENU) {
                returnToMenu();
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    /**
     * Permet de passer le bouton en état de surbrillance lorsque
     * la souris est dessus.
     *
     * @see MouseEvent
     * @param e (<code>MouseEvent</code>)
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        if(e.getSource() instanceof CustomButton) {
            CustomButton btn = (CustomButton) e.getSource();

            btn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btn.setHovered(true);
            this.g.repaint();
        }
    }

    /**
     * Permet de passer le bouton en état normal lorsque
     * la souris n'est plus dessus.
     *
     * @see MouseEvent
     * @param e (<code>MouseEvent</code>)
     */
    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() instanceof CustomButton) {
            CustomButton btn = (CustomButton) e.getSource();

            btn.setHovered(false);
            this.g.repaint();
        }
    }


    /**
     * Fonction permettant de revenir au menu principal.
     */
    public void returnToMenu() {
        JFrame frame = this.g.getFrame();
        MainMenu mainMenu = new MainMenu(frame);

        frame.setSize(900, 506);
        frame.setLocationRelativeTo(null);
        frame.setContentPane(mainMenu);
    }
}
