package fr.dehoux.controller;

import fr.dehoux.model.GameManager;
import fr.dehoux.view.Game;
import fr.dehoux.view.menu.IMenu;
import fr.dehoux.view.menu.NewGameMenu;
import fr.dehoux.view.utils.CustomButton;
import fr.dehoux.view.utils.SaveUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MenuHandler implements ActionListener, MouseListener, KeyListener {

    private JPanel panel;
    public MenuHandler(JPanel p) {
        this.panel = p;
    }

    /**
     * Exécute l'action assigné au bouton <code>CustomButton</code> lorsque celui-ci est cliqué.
     *
     * @see ActionEvent
     * @see CustomButton
     * @see CustomAction
     *
     * @param e (<code>ActionEvent</code>)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() instanceof CustomButton) {
            CustomAction action = ((CustomButton)e.getSource()).getCustomAction();
            if(action == CustomAction.CLOSE) {
                System.exit(0);
            }

            if(action == CustomAction.NEW_GAME) {
                IMenu menu = (IMenu) panel;
                NewGameMenu newMenu = new NewGameMenu(menu.getFrame());

                newMenu.setSize(menu.getFrame().getWidth(), menu.getFrame().getHeight());
                menu.getFrame().setContentPane(newMenu);
            }

            if(action == CustomAction.START_GAME) {
                IMenu menu = (IMenu) panel;
                NewGameMenu newMenu = (NewGameMenu) panel;

                int rows;
                int colons;
                int bombs;

                if(newMenu.getLignes().getText().isEmpty() || newMenu.getColonnes().getText().isEmpty() || newMenu.getBombes().getText().isEmpty()) {
                    JOptionPane.showMessageDialog(new Frame(), "Veuillez indiquer un nombre de lignes/colonnes valide, ainsi que le nombre de bombes", "Problème de lancement !", JOptionPane.ERROR_MESSAGE);
                    return;
                } else {
                    rows = Integer.parseInt(newMenu.getLignes().getText());
                    colons = Integer.parseInt(newMenu.getColonnes().getText());
                    bombs = Integer.parseInt(newMenu.getBombes().getText());

                    rows = Math.max(4, Math.min(30, rows));
                    colons = Math.max(4, Math.min(30, colons));
                    bombs = Math.max(1, Math.min(rows * colons, bombs));
                }

                GameManager manager = new GameManager(rows, colons, bombs, false);
                Game game = new Game(menu.getFrame(), manager);

                menu.getFrame().setSize(1000, 650);
                menu.getFrame().setLocationRelativeTo(null);
                menu.getFrame().setContentPane(game);
            }

            if(action == CustomAction.LOAD_GAME) {
                IMenu menu = (IMenu) panel;

                GameManager manager = SaveUtils.readGame();
                Game game = new Game(menu.getFrame(), manager);

                menu.getFrame().setSize(1000, 650);
                menu.getFrame().setLocationRelativeTo(null);
                menu.getFrame().setContentPane(game);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    /**
     * Permet de passer le bouton en état de surbrillance lorsque
     * la souris est dessus.
     *
     * @see MouseEvent
     * @param e (<code>MouseEvent</code>)
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        if(e.getSource() instanceof CustomButton) {
            CustomButton btn = (CustomButton) e.getSource();

            btn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btn.setHovered(true);
            this.panel.repaint();
        }
    }

    /**
     * Permet de passer le bouton en état normal lorsque
     * la souris n'est plus dessus.
     *
     * @see MouseEvent
     * @param e (<code>MouseEvent</code>)
     */
    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() instanceof CustomButton) {
            CustomButton btn = (CustomButton) e.getSource();

            btn.setHovered(false);
            this.panel.repaint();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) { }

    @Override
    public void keyPressed(KeyEvent e) { }

    @Override
    public void keyReleased(KeyEvent e) { }
}
