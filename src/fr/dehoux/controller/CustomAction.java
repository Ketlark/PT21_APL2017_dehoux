package fr.dehoux.controller;

import fr.dehoux.view.menu.MainMenu;
import fr.dehoux.view.menu.NewGameMenu;

/**
 * La classe <code>CustomAction</code> est une énumération permettant d'assigner une action précise à un bouton.
 *
 * @author Kevin Dehoux
 * @version 1.0
 */
public enum CustomAction {

    /**
     * Fermer la fenêtre en cours
     */
    CLOSE,

    /**
     * Réduire la fenêtre en cours
     */
    REDUCE,

    /**
     * Ouvre le panneau correspondant
     *
     * @see NewGameMenu
     */
    NEW_GAME,

    /**
     * Charge la dernière partie sauvegardé
     */
    LOAD_GAME,

    /**
     * Lance la partie
     */
    START_GAME,

    /**
     * Sauvegarde la partie en cours
     */
    SAVE_GAME,

    /**
     * Retourne au menu principal
     *
     * @see MainMenu
     */
    RETURN_MENU;
}
