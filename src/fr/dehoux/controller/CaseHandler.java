package fr.dehoux.controller;

import fr.dehoux.model.CaseManager;
import fr.dehoux.model.CaseState;
import fr.dehoux.model.GameState;
import fr.dehoux.view.Case;
import fr.dehoux.view.Game;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * La classe <code>CaseHandler</code> est utilisé comme contrôleur, elle permet d'éxécuter des actions sur les cases ainsi que la vue du jeu
 * en fonction des actions entrantes.
 *
 * @author Kevin Dehoux
 * @version 1.0
 */
public class CaseHandler implements MouseListener {

    /**
     * Vue du jeu. (<code>Game</code>)
     */
    private Game panel;

    /**
     * Manager des cases du jeu, utilisé comme modèle. (<code>CaseManager</code>)
     */
    private CaseManager caseManager;

    /**
     * Constructeur destiné à initialisé la vue et le modèle.
     *
     * @param g Vue du jeu (<code>Game</code>).
     * @param manager Manager des cases / modèle (<code>CaseManager</code>).
     */
    public CaseHandler(Game g, CaseManager manager) {
        this.panel = g;
        this.caseManager = manager;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Fonction permettant de détecter le relachement d'un clic de souris
     * qui est utilisé pour révéler une case, la faire exploser, ou changer son état.
     *
     * @param e (<code>MouseEvent</code>)
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getSource() instanceof Case) {
            if (e.getButton() == MouseEvent.BUTTON1 && this.panel.getState() == GameState.INGAME) {
                Case c = (Case) e.getSource();

                if (c.getCaseState() != CaseState.TAGGED) {
                    if (c.isMined()) {
                        c.setBlasted(true);
                        this.panel.onFinish(false);
                    } else {

                        if (!c.isClicked()) {
                            this.caseManager.getCaseRevealed().add(c);
                            c.setClicked(true);
                        }

                        if (c.getBombsNearby() == 0) {
                            this.caseManager.loopReveal(c);
                        }
                    }
                    this.panel.repaint();
                }

                if (!c.isMined() && this.caseManager.getCasesMap().size() == this.caseManager.getCaseRevealed().size() + this.panel.getGameManager().getBombs()) {
                    this.panel.onFinish(true);
                }

            } else if (e.getButton() == MouseEvent.BUTTON3 && this.panel.getState() == GameState.INGAME) {
                Case c = (Case) e.getSource();

                if (!c.isClicked()) {
                    CaseState s = CaseState.nextState(c.getCaseState());

                    if (s == CaseState.TAGGED && !this.caseManager.getCaseTagged().contains(c)) {
                        this.caseManager.getCaseTagged().add(c);
                    } else if (c.getCaseState() == CaseState.TAGGED && this.caseManager.getCaseTagged().contains(c)) {
                        this.caseManager.getCaseTagged().remove(c);
                    }

                    c.setCaseState(s);

                    int bombsTagged = this.caseManager.getCaseTagged().size();
                    if (bombsTagged >= this.panel.getGameManager().getBombs()) {
                        bombsTagged = this.panel.getGameManager().getBombs();
                    }

                    this.panel.setBombsTotal(this.panel.getGameManager().getBombs() - bombsTagged);
                    this.panel.repaint();
                }
            }
        }
    }

    /**
     * Permet de passer la case en état de surbrillance lorsque
     * la souris est dessus.
     *
     * @see MouseEvent
     * @param e (<code>MouseEvent</code>)
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() instanceof Case) {
            Case c = (Case) e.getSource();
            if (!c.isClicked()) {
                c.setHovered(true);
                this.panel.repaint();
            }
        }
    }

    /**
     * Permet de passer la case en état normal lorsque
     * la souris n'est plus dessus.
     *
     * @see MouseEvent
     * @param e (<code>MouseEvent</code>)
     */
    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() instanceof Case) {
            Case c = (Case) e.getSource();
            if (!c.isClicked()) {
                c.setHovered(false);
                this.panel.repaint();
            }
        }
    }
}
