package fr.dehoux.view;

import fr.dehoux.controller.CaseHandler;
import fr.dehoux.controller.CustomAction;
import fr.dehoux.controller.GameHandler;
import fr.dehoux.model.GameManager;
import fr.dehoux.model.GameState;
import fr.dehoux.view.utils.CustomButton;
import fr.dehoux.view.utils.LayerPanel;
import fr.dehoux.view.utils.MovePanel;
import fr.dehoux.view.utils.SaveUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Game extends MovePanel {

    /**
     * Bouton quitter.
     *
     * @see CustomAction
     * @see CustomButton
     */
    private CustomButton quit = new CustomButton(CustomAction.CLOSE, 920, 25, 55, 63, "/images/quit.png", "/images/quit_h.png");

    /**
     * Bouton sauvegarder et quitter.
     *
     * @see CustomAction
     * @see CustomButton
     */
    private CustomButton save = new CustomButton(CustomAction.SAVE_GAME, 685, 310, 280, 101, "/images/save_quit.png", "/images/save_quit_h.png");

    /**
     * Texture de case.
     */
    private Image caseImg;

    /**
     * Texture de case en état de surbrillance.
     */
    private Image caseImgHovered;

    /**
     * Texture de case révélée.
     */
    private Image caseImgRevealed;

    /**
     * Image de fond.
     */
    private Image background;

    /**
     * Image tag étoile
     */
    private Image tagged;

    /**
     * Image tag IDK
     */
    private Image flag;

    /**
     * Modèle de la partie.
     *
     * @see GameManager
     */
    private GameManager gameManager;

    /**
     * Contrôleur des cases.
     *
     * @see CaseHandler
     */
    private CaseHandler caseHandler;

    /**
     * Contrôleur de la partie.
     *
     * @see GameHandler
     */
    private GameHandler gameController;

    /**
     * Police d'écriture
     */
    private Font font = null;

    /**
     * Compteur de bombe à l'écran
     */
    private int bombsTotal;

    /**
     * Panneau de fin
     */
    private LayerPanel layerEnd;

    /**
     * Etat de la partie
     */
    private GameState state = GameState.INGAME;

    /**
     * Constructeur destiné à initialisé toutes les images ainsi que les composants graphiques.
     *
     * @param frame Fenêtre parente.
     * @param manager Modèle de la partie.
     */
    public Game(JFrame frame, GameManager manager) {
        super(frame);
        setLayout(null);
        setOpaque(false);
        setSize(1000, 650);

        try {
            background = ImageIO.read(this.getClass().getResource("/images/title.png"));

            caseImg = ImageIO.read(this.getClass().getResource("/images/case.png"));
            caseImgHovered = ImageIO.read(this.getClass().getResource("/images/case_h.png"));
            caseImgRevealed = ImageIO.read(this.getClass().getResource("/images/case_revealed.png"));
            tagged = ImageIO.read(this.getClass().getResource("/images/tagged.png"));
            flag = ImageIO.read(this.getClass().getResource("/images/flag.png"));

            this.font = Font.createFont(0, this.getClass().getResourceAsStream("/font/police.ttf")).deriveFont(Font.PLAIN, 21.0F);
        } catch (IOException e) {
            System.err.println("Impossible de charger l'image !");
        } catch (FontFormatException e) {
            System.err.println("Impossible de charger la police d'écriture !");
        }

        gameManager = manager;
        caseHandler = new CaseHandler(this, this.gameManager.caseManager);
        gameController = new GameHandler(this);

        bombsTotal = this.gameManager.getBombs();

        int offsetX = ((this.getWidth() / 2) - (this.gameManager.getRows() * 20) / 2);
        int offsetY = ((this.getHeight() / 2) - (this.gameManager.getColons() * 20) / 2);

        quit.addActionListener(gameController);
        quit.addMouseListener(gameController);
        add(quit);

        layerEnd = new LayerPanel(this);
        add(layerEnd);

        for (Case c : this.gameManager.caseManager.getCasesMap().values()) {
            c.setBounds(offsetX - 145 + (c.getCoord().getX() * 20), offsetY + (c.getCoord().getY() * 20), 20, 20);
            c.setImg(caseImg);
            c.setImgHovered(caseImgHovered);
            c.setImgRevealed(caseImgRevealed);
            c.setImgTagged(tagged);
            c.setImgFlag(flag);
            c.addMouseListener(caseHandler);
            add(c);
        }

        save.addActionListener(gameController);
        save.addMouseListener(gameController);
        add(save);

        setVisible(true);
    }


    /**
     * Fonction éxécuté lorsque la partie est finie, permet
     * de définir l'écran de fin.
     *
     * @param isVictory Boolean
     */
    public void onFinish(boolean isVictory) {
        state = GameState.FINISHED;
        if(this.gameManager.isSaveLoaded()) SaveUtils.getSaveFile().delete();
        if(isVictory) {
            state = GameState.VICTORY;

            this.layerEnd.setVisible(true);
            this.save.setEnabled(false);
            this.save.setVisible(false);
            this.repaint();
        } else {
            state = GameState.DEFEAT;
            for (Case c : this.getGameManager().caseManager.getCasesMap().values()) {
                c.setGameState(GameState.DEFEAT);
                if(!c.isMined() && !c.isClicked()) {
                    c.setClicked(true);
                }
            }

            this.layerEnd.setVisible(true);
            this.save.setEnabled(false);
            this.save.setVisible(false);
            this.repaint();
        }
    }

    /**
     * Renvoie le modèle de la partie.
     *
     * @return (<code>GameManager</code>)
     */
    public GameManager getGameManager() {
        return this.gameManager;
    }

    /**
     * Renvoie le contrôleur de la partie
     *
     * @return (<code>GameHandler</code>)
     */
    public GameHandler getActionController() {
        return this.gameController;
    }

    /**
     * Renvoie l'état de la partie.
     *
     * @return (<code>GameState</code>)
     */
    public GameState getState() {
        return state;
    }

    /**
     * Renvoie le nombre de bombes restantes à trouver.
     *
     * @return Integer
     */
    public int getBombsTotal() {
        return this.bombsTotal;
    }

    /**
     * Définit le nombre de bombes restantes à trouver.
     *
     * @param bombsTotal Integer
     */
    public void setBombsTotal(int bombsTotal) {
        this.bombsTotal = bombsTotal;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics pinceau = g.create();
        if(this.isOpaque()) {} else {
            pinceau.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);

            pinceau.setFont(font.deriveFont(Font.PLAIN, 35F));
            pinceau.setColor(new Color(174, 161, 141));
            pinceau.drawString(String.valueOf(this.getBombsTotal()), ((this.getWidth() - pinceau.getFontMetrics().stringWidth(String.valueOf(this.getBombsTotal()))) / 2) + 295, 260);
        }
    }
}
