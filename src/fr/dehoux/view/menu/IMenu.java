package fr.dehoux.view.menu;

import javax.swing.*;

/**
 * L'interface <code>IMenu</code> permet de récupérer la fenêtre parent des panneaux qui
 * en hérite.
 *
 * @author Kevin Dehoux
 * @version 1.0
 */
public interface IMenu {

    /**
     * Renvoie le JFrame parent qui contient le menu.
     *
     * @return Le container parent.
     */
    JFrame getFrame();
}
