package fr.dehoux.view.menu;

import javax.swing.*;

/**
 * La classe <code>MenuFrame</code> est la fenêtre parente de tout les menus.
 */
public class MenuFrame extends JFrame {

    /**
     * Menu principal
     *
     * @see MainMenu
     */
    private MainMenu box = new MainMenu(this);

    /**
     * Constructeur destiné à initialiser le JFrame, définir son titre, sa taille
     * sa position et ses propriétés graphiques.
     */
    public MenuFrame() {
        super("Démineur");
        setSize(900, 506);
        setLocationRelativeTo(null);
        setUndecorated(true);

        box.setSize(this.getWidth(), this.getHeight());
        setContentPane(box);
    }
}
