package fr.dehoux.view.menu;

import fr.dehoux.controller.CustomAction;
import fr.dehoux.controller.MenuHandler;
import fr.dehoux.view.utils.CustomButton;
import fr.dehoux.view.utils.MovePanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * La classe <code>MainMenu</code> est le menu principal du jeu, c'est celui que l'on retrouve dès le lancement
 * de l'application. Il hérite de la classe <code>MovePanel</code> pour pouvoir être déplacé.
 *
 * @see MovePanel
 */
public class MainMenu extends MovePanel {

    /**
     * Bouton quitter.
     *
     * @see CustomButton
     * @see CustomAction
     */
    private CustomButton quit = new CustomButton(CustomAction.CLOSE, 820, 25, 55, 63, "/images/quit.png", "/images/quit_h.png");

    /**
     * Bouton nouvelle partie.
     *
     * @see CustomButton
     * @see CustomAction
     */
    private CustomButton new_game = new CustomButton(CustomAction.NEW_GAME, 100, 100, 225, 77, "/images/new_game.png", "/images/new_game_h.png");

    /**
     * Bouton charger une partie.
     *
     * @see CustomButton
     * @see CustomAction
     */
    private CustomButton load_game = new CustomButton(CustomAction.LOAD_GAME, 300, 100, 225, 77, "/images/load_game.png", "/images/load_game_h.png");

    /**
     * Contrôleur du menu (<code>MenuHandler</code>)
     *
     * @see MenuHandler
     */
    private MenuHandler actionController;

    /**
     * Image de fond.
     */
    private Image background;

    /**
     * Constructeur du menu, il permet de définir sa fenêtre parente et d'initialiser ses composants internes
     * ainsi que ses propriétés graphiques.
     *
     * @param frame Fenêtre parente
     */
    public MainMenu(JFrame frame) {
        super(frame);

        setLayout(null);
        setOpaque(false);

        try {
            background = ImageIO.read(this.getClass().getResource("/images/menu_bg.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        actionController = new MenuHandler(this);

        load_game.addMouseListener(actionController);
        load_game.addActionListener(actionController);
        new_game.addMouseListener(actionController);
        new_game.addActionListener(actionController);
        quit.addMouseListener(actionController);
        quit.addActionListener(actionController);

        if(saveFileExist()) {
            new_game.setLocation(210, 230);
            load_game.setLocation(460, 230);
        } else {
            new_game.setLocation(335, 230);
            load_game.setVisible(false);
        }

        add(quit);
        add(new_game);
        add(load_game);

        setVisible(true);
    }

    /**
     * Retourne vrai si le fichier de sauvegarde existe, faux sinon.
     *
     * @return true/false.
     */
    public boolean saveFileExist() {
        String currentWorkingDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
        File f = new File(currentWorkingDirectory + File.separator + "game_saved.dat");
        return f.exists();
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics pinceau = g.create();
        if(this.isOpaque()) {} else {
            pinceau.setColor(Color.BLUE);
            pinceau.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
        }
    }
}
