package fr.dehoux.view.menu;

import fr.dehoux.controller.CustomAction;
import fr.dehoux.controller.MenuHandler;
import fr.dehoux.view.utils.CustomButton;
import fr.dehoux.view.utils.DigitField;
import fr.dehoux.view.utils.MovePanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * La classe <code>NewGameMenu</code> est le menu de préparation du jeu, c'est celui que l'on retrouve après
 * le menu principale, il permet de définir les caractéristiques d'une partie.
 * Il hérite de la classe <code>MovePanel</code> pour pouvoir être déplacé.
 *
 * @see MovePanel
 */
public class NewGameMenu extends MovePanel {
    /**
     * Bouton quitter.
     *
     * @see CustomAction
     * @see CustomButton
     */
    private CustomButton quit = new CustomButton(CustomAction.CLOSE, 820, 25, 55, 63, "/images/quit.png", "/images/quit_h.png");

    /**
     * Bouton lancer une partie.
     *
     * @see CustomAction
     * @see CustomButton
     */
    private CustomButton start = new CustomButton(CustomAction.START_GAME, 312, 410, 270, 97, "/images/play.png", "/images/play_h.png");

    /**
     * Champ texte lignes (Uniquement des chiffres).
     *
     * @see DigitField
     */
    private DigitField lignes = new DigitField("10");

    /**
     * Champ texte colonnes (Uniquement des chiffres).
     *
     * @see DigitField
     */
    private DigitField colonnes = new DigitField("10");

    /**
     * Champ texte bombes (Uniquement des chiffres).
     *
     * @see DigitField
     */
    private DigitField bombes = new DigitField("25");

    /**
     * Police personnalisé.
     */
    private Font font = null;

    /**
     * Fenêtre parente
     */
    public JFrame parent;

    /**
     * Image de fond
     */
    private Image background;


    /**
     * Manager / contrôleur du menu
     *
     * @see MenuHandler
     */
    private MenuHandler actionController;

    /**
     * Constructeur du menu, il permet de définir sa fenêtre parente et d'initialiser ses composants internes
     * ainsi que ses propriétés graphiques.
     *
     * @param frame Fenêtre parente
     */
    public NewGameMenu(JFrame frame) {
        super(frame);

        setLayout(null);
        setOpaque(false);

        try {
            background = ImageIO.read(this.getClass().getResource("/images/new_menu.png"));
            this.font = Font.createFont(0, this.getClass().getResourceAsStream("/font/police.ttf")).deriveFont(0, 21.0F);
        } catch (IOException | FontFormatException e) {
            System.err.println("Impossible de lire la ressource.");
        }

        actionController = new MenuHandler(this);

        lignes.setBorder(null);
        lignes.setBounds(163, 179, 60, 60);
        lignes.setBackground(new Color(0, 0, 0, 0));
        lignes.setForeground(new Color(174, 161, 144));
        lignes.setOpaque(false);
        lignes.setHorizontalAlignment(JLabel.CENTER);
        lignes.setFont(font.deriveFont(Font.BOLD, 24.0F));
        lignes.addKeyListener(actionController);

        colonnes.setBorder(null);
        colonnes.setBounds(280, 179, 60, 60);
        colonnes.setBackground(new Color(0, 0, 0, 0));
        colonnes.setForeground(new Color(174, 161, 144));
        colonnes.setOpaque(false);
        colonnes.setHorizontalAlignment(JLabel.CENTER);
        colonnes.setFont(font.deriveFont(Font.BOLD, 24.0F));
        colonnes.addKeyListener(actionController);

        bombes.setBorder(null);
        bombes.setBounds(609, 179, 60, 60);
        bombes.setBackground(new Color(0, 0, 0, 0));
        bombes.setForeground(new Color(174, 161, 144));
        bombes.setOpaque(false);
        bombes.setHorizontalAlignment(JLabel.CENTER);
        bombes.setFont(font.deriveFont(Font.BOLD, 21.0F));
        bombes.setId(2);
        bombes.addKeyListener(actionController);

        quit.addMouseListener(actionController);
        quit.addActionListener(actionController);
        start.addMouseListener(actionController);
        start.addActionListener(actionController);

        add(quit);
        add(lignes);
        add(colonnes);
        add(bombes);
        add(start);
        setVisible(true);
    }

    /**
     * Retourne le nombre de lignes.
     *
     * @return Nombre de lignes contenu dans le champ texte.
     */
    public DigitField getLignes() {
        return lignes;
    }

    /**
     * Retourne le nombre de colonnes.
     *
     * @return Nombre de colonnes contenu dans le champ texte.
     */
    public DigitField getColonnes() {
        return colonnes;
    }

    /**
     * Retourne le nombre de bombes.
     *
     * @return Nombre de bombes contenu dans le champ texte.
     */
    public DigitField getBombes() {
        return bombes;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics pinceau = g.create();
        if(this.isOpaque()) {} else {
            pinceau.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), null);
        }
    }
}
