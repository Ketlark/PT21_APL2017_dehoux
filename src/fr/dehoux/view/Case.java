package fr.dehoux.view;

import fr.dehoux.model.CaseCoordinate;
import fr.dehoux.model.CaseState;
import fr.dehoux.model.GameState;

import javax.swing.*;
import java.awt.*;

public class Case extends JComponent {

    /**
     * Coordonnées de la case sous forme d'un <code>CaseCoordinate</code>.
     */
    private CaseCoordinate coord;

    /**
     * Nombre de bombes autour de la case.
     */
    private int bombsNearby;

    /**
     * Est miné.
     */
    private boolean mined;

    /**
     * Est révélée.
     */
    private boolean clicked;

    /**
     * Est dans un état de surbrillance.
     */
    private boolean hovered;

    /**
     * Est explosé.
     */
    private boolean blasted = false;

    /**
     * Etat de la partie en cours par défaut.
     */
    private GameState gameState = GameState.INGAME;

    /**
     * Etat tag de la case par défaut
     */
    private CaseState caseState = CaseState.NONE;

    /**
     * Texture de la case.
     */
    private Image img;

    /**
     * Texture de surbrillance.
     */
    private Image imgHovered;

    /**
     * Texture de la case quand elle est révélée.
     */
    private Image imgRevealed;

    /**
     * Texture de la case quand elle est en état de tag IDK
     *
     * @see CaseState
     */
    private Image imgFlag;

    /**
     * Texture de la case quand elle est en état de tag TAGGED
     *
     * @see CaseState
     */
    private Image imgTagged;

    /**
     * Construteur permettant d'initialisé une case avec ses coordonnées, ses propriétés graphiques ect ...
     *
     * @param x Position X.
     * @param y Position Y.
     */
    public Case(int x, int y) {
        this.setBounds(x * 20, y * 20, 20, 20);
        this.setOpaque(false);

        this.setCoord(new CaseCoordinate(x, y));
    }

    /**
     * Renvoie l'objet <code>CaseCoordinate</code> d'une case.
     *
     * @return Coordonnées de la case (<code>CaseCoordinate</code>).
     */
    public CaseCoordinate getCoord() {
        return coord;
    }

    /**
     * Définit les coordonnées d'une case sous forme d'un objet (<code>CaseCoordinate</code>).
     *
     * @param coord (<code>CaseCoordinate</code>).
     */
    public void setCoord(CaseCoordinate coord) {
        this.coord = coord;
    }

    /**
     * Renvoie si la mine est minée ou pas.
     *
     * @return Boolean
     */
    public boolean isMined() {
        return this.mined;
    }

    /**
     * Définit si une mine est minée ou pas.
     *
     * @param mined Boolean
     */
    public void setMined(boolean mined) {
        this.mined = mined;
    }

    /**
     * Renvoie le nombre de bombes autour de la case.
     *
     * @return Nombre de bombes
     */
    public int getBombsNearby() {
        return bombsNearby;
    }

    /**
     * Définit le nombre de bombes autour de la case.
     *
     * @param bombsNearby Integer
     */
    public void setBombsNearby(int bombsNearby) {
        this.bombsNearby = bombsNearby;
    }

    /**
     * Incrément le nombre de bombes autour de la case.
     */
    public void addBombsNearby() {
        this.bombsNearby++;
    }

    /**
     * Renvoie si la case est révélée ou pas.
     *
     * @return Boolean
     */
    public boolean isClicked() {
        return clicked;
    }

    /**
     * Définit si une case est révélée ou pas.
     *
     * @param clicked Boolean
     */
    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    /**
     * Renvoie si la case est explosée ou pas.
     *
     * @return Boolean
     */
    public boolean isBlasted() {
        return blasted;
    }

    /**
     * Définit si la case est explosée ou pas.
     *
     * @param blasted Boolean
     */
    public void setBlasted(boolean blasted) {
        this.blasted = blasted;
    }

    /**
     * Renvoie si la case est en état de surbrillance ou pas.
     *
     * @return Boolean
     */
    public boolean isHovered() {
        return hovered;
    }

    /**
     * Définit si la case est en état de surbrillance ou pas.
     *
     * @param hovered Boolean
     */
    public void setHovered(boolean hovered) {
        this.hovered = hovered;
    }

    /**
     * Définit la texture de base de la case.
     *
     * @param img Image
     */
    public void setImg(Image img) {
        this.img = img;
    }

    /**
     * Définit la texture en état de surbrillance de la case.
     *
     * @param imgHovered Image
     */
    public void setImgHovered(Image imgHovered) {
        this.imgHovered = imgHovered;
    }

    /**
     * Définit la texture révélée de la case.
     *
     * @param imgRevealed Image
     */
    public void setImgRevealed(Image imgRevealed) {
        this.imgRevealed = imgRevealed;
    }

    /**
     * Définit la texture en état tag "étoile" de la case.
     *
     * @param flag Image
     */
    public void setImgFlag(Image flag) {
        this.imgFlag = flag;
    }

    /**
     * Définit la texture en état IDK de la case.
     *
     * @param imgTagged Image
     */
    public void setImgTagged(Image imgTagged) {
        this.imgTagged = imgTagged;
    }

    /**
     * Renvoie l'état tag de la case.
     *
     * @return (<code>CaseState</code>)
     */
    public CaseState getCaseState() {
        return caseState;
    }

    /**
     * Définit l'état de tag de la case.
     *
     * @param caseState (<code>CaseState</code>)
     */
    public void setCaseState(CaseState caseState) {
        this.caseState = caseState;
    }

    /**
     * Renvoie l'état de la partie.
     *
     * @return (<code>GameState</code>)
     */
    public GameState getGameState() {
        return gameState;
    }

    /**
     * Définit l'état de la partie sur la case.
     *
     * @param gameState GameState
     */
    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    /**
     * Renvoie une couleur spécifique en fonction du nombre de bombes
     * autour de la case.
     *
     * @return (<code>Color</code>)
     */
    private Color getColorWithBombs() {
        if(this.getBombsNearby() <= 3) {
            return new Color(174, 161, 141);
        } else if(this.getBombsNearby() > 3 && this.getBombsNearby() <= 6) {
            return new Color(192, 138, 62);
        } else {
            return new Color(237, 91, 0);
        }
    }

    /**
     * Permet de tracer un rectangle coloré semi-transparent sur une case.
     *
     * @param g Le pinceau (<code>Graphics</code>)
     * @param c (<code>Color</code>)
     */
    private void layerRect(Graphics g, Color c) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setComposite(AlphaComposite.SrcOver.derive(0.5F));
        g2d.setColor(c);
        g2d.fillRect(0, 0, 20, 20);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics pinceau = g.create();
        pinceau.setFont(new Font("Roboto Mono", Font.PLAIN, 19));
        pinceau.setColor(this.getColorWithBombs());

        if(this.isOpaque()) {} else {
            if(this.isClicked()) {
                pinceau.drawImage(imgRevealed, 0, 0, this.getWidth(), this.getHeight(), null);
                if(this.getBombsNearby() != 0) {
                    pinceau.drawString(String.valueOf(this.getBombsNearby()), 4, 16);
                }
            } else {
                if(this.getGameState() == GameState.DEFEAT && this.isBlasted()) this.layerRect(pinceau, Color.red);
                if(this.isHovered()) {
                    pinceau.drawImage(imgHovered, 0, 0, this.getWidth(), this.getHeight(), null);
                } else {
                    pinceau.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), null);
                }

                if(this.getCaseState() == CaseState.TAGGED) {
                    if(this.getGameState() == GameState.DEFEAT) this.layerRect(pinceau, Color.magenta);
                    pinceau.drawImage(imgTagged, 2, 2, 16, 16, null);
                } else if(this.getCaseState() == CaseState.IDK) {
                    pinceau.drawImage(imgFlag, 3, 1, 14, 17, null);
                }

                if(this.getGameState() == GameState.DEFEAT && this.isMined() && !this.isBlasted() && this.getCaseState() != CaseState.TAGGED) this.layerRect(pinceau, Color.cyan);
            }
        }
    }
}
