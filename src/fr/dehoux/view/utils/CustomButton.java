package fr.dehoux.view.utils;

import fr.dehoux.controller.CustomAction;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * La classe <code>CustomButton</code> hérite de JButton, elle permet de créer
 * des boutons personnalisés, ayant chacun une action assigné.
 *
 * @see CustomAction
 */
public class CustomButton extends JButton {

    /**
     * Image état normal
     */
    private Image img;

    /**
     * Image état surbrillance
     */
    private Image imgHover;

    /**
     * Action du bouton.
     *
     * @see CustomAction
     */
    private CustomAction action;

    /**
     * Est en surbrillance ou non
     */
    private boolean hovered;

    /**
     * Constructeur qui permet de définir toutes les caractéristiques du bouton.
     *
     * @param action Action du bouton
     * @param x Position X.
     * @param y Position Y.
     * @param w Largeur du bouton.
     * @param h Hauteur du bouton.
     * @param file Chemin des textures.
     */
    public CustomButton(CustomAction action, int x, int y, int w, int h, String... file) {
        setBounds(x, y, w, h);
        setBorderPainted(false);
        setVisible(true);
        setBackground(new Color(0, 0, 0, 0));
        setOpaque(false);

        this.action = action;
        try {
            this.img = ImageIO.read(this.getClass().getResource(file[0]));
            this.imgHover = ImageIO.read(this.getClass().getResource(file[1]));
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Renvoie l'action assigné au bouton.
     *
     * @return L'action du bouton.
     */
    public CustomAction getCustomAction() {
        return this.action;
    }

    /**
     * Renvoie vrai si en surbrillance, sinon non
     *
     * @return true/false.
     */
    public boolean isHovered() {
        return hovered;
    }

    /**
     * Permet de définir l'état d'un bouton, surbrillance ou normal (true/false)
     *
     * @param hovered Boolean
     */
    public void setHovered(boolean hovered) {
        this.hovered = hovered;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics pinceau = g.create();
        if(this.isOpaque()) {} else {
            if(this.isHovered()) {
                pinceau.drawImage(imgHover, 0, 0, getWidth(), getHeight(), this);
            } else {
                pinceau.drawImage(img, 0, 0, getWidth(), getHeight(), this);
            }
        }
    }
}
