package fr.dehoux.view.utils;

import fr.dehoux.controller.CustomAction;
import fr.dehoux.model.GameState;
import fr.dehoux.view.Game;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * La classe <code>LayerPanel</code> est un {@link JPanel} utilisé pour apparaître à la fin d'une partie pour indiqué la victoire ou la défaite.
 */
public class LayerPanel extends JPanel {


    /**
     * Bouton retour au menu.
     *
     * @see CustomButton
     * @see CustomAction
     */
    private CustomButton returnBtn = new CustomButton(CustomAction.RETURN_MENU, 361, 435, 275, 95, "/images/return_menu.png", "/images/return_menu_h.png");

    /**
     * Image de fond.
     */
    private Image layerFinished;

    /**
     * Image de victoire.
     */
    private Image victory;

    /**
     * Image de défaite.
     */
    private Image defeat;

    /**
     * Vue du jeu
     */
    private Game panel;

    /**
     * Constructeur destiné à initialisé le panneau avec en paramètre la vue du jeu.
     *
     * @param p Vue/Panneau du jeu (<code>Game</code>)
     */
    public LayerPanel(Game p) {
        this.panel = p;
        setSize(p.getWidth(), p.getHeight());
        setLayout(null);
        setOpaque(false);

        try {
            layerFinished = ImageIO.read(this.getClass().getResource("/images/layer_finished.png"));
            victory = ImageIO.read(this.getClass().getResource("/images/victory.png"));
            defeat = ImageIO.read(this.getClass().getResource("/images/defeat.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        returnBtn.addActionListener(this.panel.getActionController());
        returnBtn.addMouseListener(this.panel.getActionController());
        add(returnBtn);

        setVisible(false);
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics pinceau = g.create();
        if(this.isOpaque()) {} else {
            if(this.panel.getState() != GameState.INGAME) {
                pinceau.drawImage(layerFinished, 0, 0, this.getWidth(), this.getHeight(), null);
                if(panel.getState() == GameState.VICTORY) {
                    pinceau.drawImage(victory, (this.getWidth() - this.victory.getWidth(null)) / 2, (this.getHeight() - this.victory.getHeight(null)) / 2, this.victory.getWidth(null), this.victory.getHeight(null), null);
                } else {
                    pinceau.drawImage(defeat, (this.getWidth() - this.defeat.getWidth(null)) / 2, (this.getHeight() - this.defeat.getHeight(null)) / 2, this.defeat.getWidth(null), this.defeat.getHeight(null), null);
                }
            }
        }
    }
}
