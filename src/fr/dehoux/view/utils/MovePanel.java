package fr.dehoux.view.utils;

import fr.dehoux.view.menu.IMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

/**
 * La classe <code>MovePanel</code> est un JPanel modifié qui peut être déplacé en restant cliqué dessus. Il hérite
 * aussi de l'interface <code>IMenu</code> pour récupérer sa fenêtre parente.
 */
public class MovePanel extends JPanel implements IMenu {
    /**
     * Point initial.
     */
    private Point initialClick;

    /**
     * Fenêtre parente.
     */
    private JFrame parent;

    /**
     * Constructeur du menu, il permet de définir sa fenêtre parente et d'initialiser ses composants internes
     * ainsi que ses propriétés graphiques.
     *
     * @param frame Fenêtre parente
     */
    public MovePanel(JFrame frame) {
        this.parent = frame;

        addMouseListener(new MouseAdapter()
        {
            public void mousePressed(MouseEvent e)
            {
                MovePanel.this.initialClick = e.getPoint();
                MovePanel.this.getComponentAt(MovePanel.this.initialClick);
            }
        });
        addMouseMotionListener(new MouseMotionAdapter()
        {
            public void mouseDragged(MouseEvent e)
            {
                int thisX = parent.getLocation().x;
                int thisY = parent.getLocation().y;


                int xMoved = thisX + e.getX() - (thisX + MovePanel.this.initialClick.x);
                int yMoved = thisY + e.getY() - (thisY + MovePanel.this.initialClick.y);


                int X = thisX + xMoved;
                int Y = thisY + yMoved;
                parent.setLocation(X, Y);
            }
        });
    }

    /**
     * Renvoie la fenêtre parente.
     *
     * @return La fenêtre parente.
     */
    @Override
    public JFrame getFrame() {
        return this.parent;
    }
}
