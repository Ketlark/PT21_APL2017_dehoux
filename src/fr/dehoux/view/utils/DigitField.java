package fr.dehoux.view.utils;

import javax.swing.*;
import java.awt.event.KeyEvent;

import static java.awt.event.KeyEvent.VK_BACK_SPACE;

/**
 * A {@link JTextField} that skips all non-digit keys. The user is only able to enter numbers.
 *
 * @author Michi Gysel <michi@scythe.ch>
 *
 */
public class DigitField extends JTextField {
    private static final long serialVersionUID = 1L;

    /**
     * ID du champ texte.
     */
    private int id;

    /**
     * Constructeur permettant de définir le contenu par défaut
     * du champ texte.
     *
     * @param text Texte par défaut
     */
    public DigitField(String text) {
        super(text);
    }

    @Override
    public void processKeyEvent(KeyEvent ev) {
        if (Character.isDigit(ev.getKeyChar()) || ev.getKeyCode() == VK_BACK_SPACE) {
            super.processKeyEvent(ev);
        }
        ev.consume();
        return;
    }

    /**
     * Renvoie l'ID du champ texte.
     *
     * @return L'ID du champ texte.
     */
    public int getId() {
        return id;
    }

    /**
     * Permet de définir l'ID du champ texte.
     *
     * @param id L'ID du champ texte.
     */
    public void setId(int id) {
        this.id = id;
    }
}