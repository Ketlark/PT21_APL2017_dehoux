package fr.dehoux.view.utils;

import fr.dehoux.model.CaseState;
import fr.dehoux.model.GameManager;
import fr.dehoux.model.GameState;
import fr.dehoux.view.Case;
import fr.dehoux.view.Game;

import java.io.*;
import java.nio.file.Paths;


/**
 * La classe <code>SaveUtils</code> contient différentes méthodes permettant de sauvegarder charger une partie dans son intégralité.
 */
public class SaveUtils {


    /**
     * Renvoie le fichier de sauvegarde
     *
     * @return Fichier de sauvegarde
     */
    public static File getSaveFile() {
        String currentWorkingDirectory = Paths.get(".").toAbsolutePath().normalize().toString();
        return new File(currentWorkingDirectory + File.separator + "game_saved.dat");
    }


    /**
     * Sauvegarde la partie en cours
     *
     * @param game Vue/Panneau du jeu (<code>Game</code>)
     */
    public static void saveGame(Game game) {

        try {
            FileOutputStream fluxSave = new FileOutputStream(getSaveFile());

            try {
                ObjectOutputStream dataOut = new ObjectOutputStream(fluxSave);

                dataOut.writeInt(game.getGameManager().getRows());
                dataOut.writeInt(game.getGameManager().getColons());
                dataOut.writeInt(game.getBombsTotal());
                for (Case c : game.getGameManager().caseManager.getCasesMap().values()) {
                    dataOut.writeInt(c.getCoord().getX());
                    dataOut.writeInt(c.getCoord().getY());
                    dataOut.writeInt(c.getBombsNearby());
                    dataOut.writeBoolean(c.isMined());
                    dataOut.writeBoolean(c.isClicked());
                    dataOut.writeObject(c.getGameState());
                    dataOut.writeObject(c.getCaseState());
                }

                dataOut.flush();
                dataOut.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Renvoie un <code>GameManager</code> contenant les données sauvegardées de la partie.
     *
     * @return <code>GameManager</code>
     */
    public static GameManager readGame() {

        GameManager gameManager = null;
        int rows;
        int colons;
        int bombs;

        try {
            FileInputStream flux = new FileInputStream(getSaveFile());

            try {
                ObjectInputStream dataIn = new ObjectInputStream(flux);
                
                rows = dataIn.readInt();
                colons = dataIn.readInt();
                bombs = dataIn.readInt();

                gameManager = new GameManager(rows, colons, bombs, true);

                while(dataIn.available() > 0) {
                    Case c = new Case(dataIn.readInt(), dataIn.readInt());
                    c.setBombsNearby(dataIn.readInt());
                    c.setMined(dataIn.readBoolean());
                    c.setClicked(dataIn.readBoolean());
                    try {
                        c.setGameState((GameState) dataIn.readObject());
                        c.setCaseState((CaseState) dataIn.readObject());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    gameManager.caseManager.registerCase(c);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                flux.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        return gameManager;
    }
}
